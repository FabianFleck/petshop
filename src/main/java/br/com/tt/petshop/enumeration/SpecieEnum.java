package br.com.tt.petshop.enumeration;

public enum SpecieEnum {

    REPTILE("Réptil"),
    MAMMAL("Mamífero"),
    FISH("Peixe");

    private final String label;

    SpecieEnum(final String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return "SpecieEnum{"
                + "name='" + label + '\''
                + '}';
    }
//    @JsonCreator
    public static SpecieEnum valueOfLabel(String label) {
        for (SpecieEnum e : values()) {
            if (e.label.equals(label)) {
                return e;
            }
        }
        return null;
    }
}
