package br.com.tt.petshop.converter.client;

import br.com.tt.petshop.model.dto.client.ClientRequestDTO;
import br.com.tt.petshop.model.entity.Client;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class ClientRequestDTOToClientConverter implements Converter<ClientRequestDTO, Client> {

    private final ModelMapper modelMapper;

    public ClientRequestDTOToClientConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Client convert(ClientRequestDTO clientRequestDTO) {
        Client client = modelMapper.map(clientRequestDTO, Client.class);
        if (Objects.isNull(client)) {
            throw new IllegalArgumentException("Objeto ClientRequestDTO não pode ser convertido em Client");
        }
        return client;
    }
}
