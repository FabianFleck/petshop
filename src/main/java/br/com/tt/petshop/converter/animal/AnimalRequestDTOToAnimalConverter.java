package br.com.tt.petshop.converter.animal;

import br.com.tt.petshop.model.dto.animal.AnimalRequestDTO;
import br.com.tt.petshop.model.entity.Animal;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class AnimalRequestDTOToAnimalConverter implements Converter<AnimalRequestDTO, Animal> {

    private final ModelMapper modelMapper;

    public AnimalRequestDTOToAnimalConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Animal convert(AnimalRequestDTO animalRequestDTO) {
        Animal animal = modelMapper.map(animalRequestDTO, Animal.class);
        if (Objects.isNull(animal)) {
            throw new IllegalArgumentException("Objeto AnimalRequestDTO não pode ser convertido em Animal");
        }
        return animal;
    }
}
