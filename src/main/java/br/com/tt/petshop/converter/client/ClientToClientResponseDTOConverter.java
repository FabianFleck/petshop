package br.com.tt.petshop.converter.client;

import br.com.tt.petshop.model.dto.client.ClientResponseDTO;
import br.com.tt.petshop.model.entity.Client;
import org.springframework.core.convert.converter.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class ClientToClientResponseDTOConverter implements Converter<Client, ClientResponseDTO> {

    private final ModelMapper modelMapper;

    public ClientToClientResponseDTOConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public ClientResponseDTO convert(Client client) {
        ClientResponseDTO clientResponseDTO = modelMapper.map(client, ClientResponseDTO.class);
        if (Objects.isNull(clientResponseDTO)) {
            throw new IllegalArgumentException("Objeto Client não pode ser convertido em ClientDTO");
        }
        return clientResponseDTO;
    }
}
