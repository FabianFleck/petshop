package br.com.tt.petshop.converter.animal;

import br.com.tt.petshop.model.dto.animal.AnimalResponseDTO;
import br.com.tt.petshop.model.entity.Animal;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class AnimalToAnimalResponseDTOConverter implements Converter<Animal, AnimalResponseDTO>{

    private final ModelMapper modelMapper;

    public AnimalToAnimalResponseDTOConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public AnimalResponseDTO convert(Animal animal) {
        AnimalResponseDTO animalResponseDTO = modelMapper.map(animal, AnimalResponseDTO.class);
        if (Objects.isNull(animalResponseDTO)) {
            throw new IllegalArgumentException("Objeto Animal não pode ser convertido em AnimalDTO");
        }
        return animalResponseDTO;
    }
}
