package br.com.tt.petshop.service;

import br.com.tt.petshop.model.entity.Unity;
import br.com.tt.petshop.repository.UnityRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UnityService {

    private final UnityRepository unityRepository;

    public UnityService(UnityRepository unityRepository) {
        this.unityRepository = unityRepository;
    }

    public Unity save(Unity unity) {
        return unityRepository.save(unity);
    }

    public void deleteById(Long id) {
        unityRepository.deleteById(id);
    }

    public List<Unity> findAll() {
        return unityRepository.findAll();
    }

    public Optional<Unity> findById(Long id) {
        return unityRepository.findById(id);
    }

    public void update(Unity unity) {
        unityRepository.saveAndFlush(unity);
    }
}
