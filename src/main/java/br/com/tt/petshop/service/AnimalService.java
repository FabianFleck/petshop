package br.com.tt.petshop.service;

import br.com.tt.petshop.converter.animal.AnimalToAnimalResponseDTOConverter;
import br.com.tt.petshop.enumeration.SpecieEnum;
import br.com.tt.petshop.exception.BusinessException;
import br.com.tt.petshop.model.dto.animal.AnimalResponseDTO;
import br.com.tt.petshop.model.entity.Animal;
import br.com.tt.petshop.repository.AnimalRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AnimalService {

    private final AnimalToAnimalResponseDTOConverter animalToAnimalResponseDTOConverter;
    private final AnimalRepository animalRepository;
    private final ClientService clientService;

    public AnimalService(AnimalToAnimalResponseDTOConverter animalToAnimalResponseDTOConverter, AnimalRepository animalRepository, ClientService clientService) {
        this.animalToAnimalResponseDTOConverter = animalToAnimalResponseDTOConverter;
        this.animalRepository = animalRepository;
        this.clientService = clientService;
    }

    public List<Animal> findAll() {
        return animalRepository.findAll();
    }

    public AnimalResponseDTO findById(Long id) {
        Optional<Animal> animal = animalRepository.findById(id);
        if (!animal.isPresent()) {
            throw new IllegalArgumentException("Error: Animal not found!");
        }
        return animalToAnimalResponseDTOConverter.convert(animal.get());
    }

    public List<Animal> findByClientId(Long clientId) {
        return animalRepository.findByClientId(clientId);
    }

    public AnimalResponseDTO save(Animal animal) throws BusinessException {
        AnimalResponseDTO animalSave;
        if(animal != null) {
            validateAnimalsName(animal);
            validateAnimalBirthday(animal);
            clientService.validateClientDefaulting(animal.getClient().getId());
            animalSave = animalToAnimalResponseDTOConverter.convert(animalRepository.save(animal));
        } else {
            throw new IllegalArgumentException("Error: Animal can't be saved!");
        }
        return animalSave;
    }

    public void delete(Animal animal) {
        animalRepository.delete(animal);
    }

    public List<String> listSpecieEnum() {
        List<String> specieEnumList = new ArrayList<>();
        for (SpecieEnum specieEnum : SpecieEnum.values()) {
            specieEnumList.add(specieEnum.getLabel());
        }
        return specieEnumList;
    }

    private void validateAnimalsName(Animal animal) throws BusinessException {
        if(animal.getName().length() <= 3) {
            throw new BusinessException("The animal's name is wrong!");
        }
    }

    private void validateAnimalBirthday(Animal animal) throws BusinessException {
        if(animal.getBirthDate().isAfter(LocalDate.now())) {
            throw new BusinessException("The animal's birthday is wrong!");
        }
    }

    public void update(Animal animal) {
        animalRepository.saveAndFlush(animal);
    }

    public void deleteById(Long id) {
        animalRepository.deleteById(id);
    }
}
