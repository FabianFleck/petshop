package br.com.tt.petshop.service;

import br.com.tt.petshop.converter.client.ClientRequestDTOToClientConverter;
import br.com.tt.petshop.converter.client.ClientToClientResponseDTOConverter;
import br.com.tt.petshop.exception.BusinessException;
import br.com.tt.petshop.integration.CreditClientIntegration;
import br.com.tt.petshop.integration.dto.CreditDTO;
import br.com.tt.petshop.model.dto.client.ClientRequestDTO;
import br.com.tt.petshop.model.dto.client.ClientResponseDTO;
import br.com.tt.petshop.model.entity.Client;
import br.com.tt.petshop.repository.ClientRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClientService {

    private final ClientRequestDTOToClientConverter clientRequestDTOToClientConverter;
    private final ClientToClientResponseDTOConverter clientToClientResponseDTOConverter;
    private final ClientRepository clientRepository;
    private final CreditClientIntegration creditClient;

    public ClientService(ClientRequestDTOToClientConverter clientRequestDTOToClientConverter, ClientToClientResponseDTOConverter clientToClientResponseDTOConverter, ClientRepository clientRepository, CreditClientIntegration creditClient) {
        this.clientRequestDTOToClientConverter = clientRequestDTOToClientConverter;
        this.clientToClientResponseDTOConverter = clientToClientResponseDTOConverter;
        this.clientRepository = clientRepository;
        this.creditClient = creditClient;
    }

    public List<ClientResponseDTO> findAll() {
        return clientRepository.findAll()
                .stream()
                .map(clientToClientResponseDTOConverter::convert)
                .collect(Collectors.toList());
    }

    public ClientResponseDTO findById(Long id) {
        Optional<Client> client = clientRepository.findById(id);
        if (!client.isPresent()) {
            throw new IllegalArgumentException("Error: Client not found!");
        }
        return clientToClientResponseDTOConverter.convert(client.get());
    }

    public void delete(Long clientId) {
        Client client = new Client(clientId);
        clientRepository.delete(client);
    }

    public ClientResponseDTO save(ClientRequestDTO requestDTO) throws BusinessException {
        validateName(requestDTO.getName());
        validateCPF(requestDTO.getCpf());
        validSituationClient(requestDTO.getCpf());
        Client client = clientRequestDTOToClientConverter.convert(requestDTO);
        client.setDefaulting(false);
        return clientToClientResponseDTOConverter.convert(clientRepository.save(client));
    }

    public CreditDTO validSituationClient(String cpf) throws BusinessException {
        return creditClient.verifyCredit(cpf);
    }

    public void deleteById(Long id) {
        clientRepository.deleteById(id);
    }

    public void update(Client client) {
        clientRepository.saveAndFlush(client);
    }

    private void validateName(String clientName) throws BusinessException {

        String NAME_DIVISOR = " ";
        if (clientName.trim().contains(NAME_DIVISOR)) {
            String[] nameAndSurname = clientName.split(NAME_DIVISOR);
            for (String name : nameAndSurname) {
                if (name.length() < 2) {
                    throw new BusinessException("The name should have 2 characters");
                }
            }
        } else {
            throw new BusinessException("Name must have a last name");
        }
    }

    private void validateCPF(String clientCPF) throws BusinessException {
        if (clientCPF.replaceAll("[^0-9]", "").length() != 11) {
            throw new BusinessException("The CPF should have eleven characters!");
        }
    }

    void validateClientDefaulting(Long clientId) throws BusinessException {
        Optional<Client> client = clientRepository.findById(clientId);
        if (client.isPresent() && Boolean.TRUE.equals(client.get().getDefaulting())) {
            throw new BusinessException("Client is defaulting!");
        }
    }
}
