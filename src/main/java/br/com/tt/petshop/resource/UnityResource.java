package br.com.tt.petshop.resource;

import br.com.tt.petshop.model.entity.Unity;
import br.com.tt.petshop.service.UnityService;
import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@Api(description = "API REST Unity", tags = "Unity")
@RestController
@RequestMapping("/api/petshop/unity")
public class UnityResource {

    private final UnityService unityService;

    public UnityResource(UnityService unityService) {
        this.unityService = unityService;
    }

    @PostMapping
    public ResponseEntity save(@RequestBody Unity unity) {
        Unity unitySave = unityService.save(unity);
        URI uri = URI.create("/api/petshop/unity/" + unitySave.getId());
        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        unityService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping
    public ResponseEntity update(@RequestBody Unity unity) {
        unityService.update(unity);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<List<Unity>> findAll() {
        return ResponseEntity.ok(unityService.findAll());
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Unity> findById(@PathVariable Long id) {
        Optional<Unity> unity = unityService.findById(id);
        if (unity.isPresent()) {
            return ResponseEntity.ok(unity.get());
        }
        return ResponseEntity.notFound().build();
    }
}
