package br.com.tt.petshop.resource;

import br.com.tt.petshop.exception.BusinessException;
import br.com.tt.petshop.integration.dto.CreditDTO;
import br.com.tt.petshop.model.dto.client.ClientRequestDTO;
import br.com.tt.petshop.model.dto.client.ClientResponseDTO;
import br.com.tt.petshop.model.entity.Client;
import br.com.tt.petshop.service.ClientService;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;

@Api(description = "API REST Client", tags = "Client")
@RestController
@RequestMapping("/api/petshop/client")
public class ClientResource {

    private final ClientService clientService;

    public ClientResource(ClientService clientService) {
        this.clientService = clientService;
    }

    @PostMapping
    public ResponseEntity save(@RequestBody ClientRequestDTO clientRequestDTO) throws BusinessException {
        ClientResponseDTO clientResponseDTO = clientService.save(clientRequestDTO);
        URI uri = URI.create("/api/petshop/client/" + clientResponseDTO.getId());
        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        clientService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping
    public ResponseEntity update(@RequestBody Client client) {
        clientService.update(client);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<List<ClientResponseDTO>> findAll() {
        return ResponseEntity.ok(clientService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ClientResponseDTO> findById(@PathVariable Long id) {
        return ResponseEntity.ok(clientService.findById(id));
    }


    @GetMapping("credit/{cpf}")
    public ResponseEntity<CreditDTO> creditClient(@PathVariable String cpf) throws BusinessException {
        return ResponseEntity.ok(clientService.validSituationClient(cpf));
    }
}
