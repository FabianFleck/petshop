package br.com.tt.petshop.resource;

import br.com.tt.petshop.exception.BusinessException;
import br.com.tt.petshop.model.dto.animal.AnimalResponseDTO;
import br.com.tt.petshop.model.entity.Animal;
import br.com.tt.petshop.model.entity.Client;
import br.com.tt.petshop.service.AnimalService;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;

@Api(description = "API REST Animal", tags = "Animal")
@RestController
@RequestMapping("/api/petshop/animal")
public class AnimalResource {

    private final AnimalService animalService;

    public AnimalResource(AnimalService animalService) {
        this.animalService = animalService;
    }

    @PostMapping
    public ResponseEntity save(@RequestBody Animal animal) throws BusinessException {
        animal.setClient(new Client(1L));
        AnimalResponseDTO animalResponseDTO = animalService.save(animal);
        URI uri = URI.create("/api/petshop/animal/" + animalResponseDTO.getId());
        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        animalService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<List<Animal>> findAll() {
        return ResponseEntity.ok(animalService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<AnimalResponseDTO> findById(@PathVariable Long id) {
        return ResponseEntity.ok(animalService.findById(id));
    }

    @PutMapping
    public ResponseEntity update(@RequestBody Animal animal) {
        animalService.update(animal);
        return ResponseEntity.ok().build();
    }
}
