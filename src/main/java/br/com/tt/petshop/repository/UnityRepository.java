package br.com.tt.petshop.repository;

import br.com.tt.petshop.model.entity.Unity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UnityRepository extends JpaRepository<Unity, Long> {
}
