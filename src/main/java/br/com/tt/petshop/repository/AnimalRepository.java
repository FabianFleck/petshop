package br.com.tt.petshop.repository;

import br.com.tt.petshop.model.entity.Animal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnimalRepository extends JpaRepository<Animal, Long> {

    List<Animal> findByClientId(Long clientId);

    List<Animal> findByName(String name);
}
