package br.com.tt.petshop.model.dto.animal;

import br.com.tt.petshop.enumeration.SpecieEnum;

import java.time.LocalDate;

public class AnimalResponseDTO {

    private Long id;
    private String name;
    private LocalDate birthDate;
    private SpecieEnum specieEnum;

    public AnimalResponseDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public SpecieEnum getSpecieEnum() {
        return specieEnum;
    }

    public void setSpecieEnum(SpecieEnum specieEnum) {
        this.specieEnum = specieEnum;
    }
}
