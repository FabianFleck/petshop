package br.com.tt.petshop.model.dto.client;

public class ClientRequestDTO {

    private String cpf;
    private String name;

    public ClientRequestDTO() {
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
