package br.com.tt.petshop.model.dto.client;

import br.com.tt.petshop.model.dto.animal.AnimalResponseDTO;

import java.util.List;

public class ClientResponseDTO {

    private Long id;
    private String name;
    private String cpf;
    private List<AnimalResponseDTO> animals;

    public ClientResponseDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public List<AnimalResponseDTO> getAnimals() {
        return animals;
    }

    public void setAnimals(List<AnimalResponseDTO> animals) {
        this.animals = animals;
    }
}
