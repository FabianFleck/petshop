package br.com.tt.petshop.model.entity;

import br.com.tt.petshop.enumeration.SpecieEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TA_ANIMAL")
public class Animal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "ANIMAL_NAME")
    private String name;

    @Column(name = "ANIMAL_BIRTH_DATE")
    private LocalDate birthDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "ANIMAL_SPECIE")
    private SpecieEnum specieEnum;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "ID_CLIENT")
    private Client client;

    @ManyToOne
    @JoinColumn(name = "ID_UNITY")
    private Unity unity;

    public Animal() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public SpecieEnum getSpecieEnum() {
        return specieEnum;
    }

    public void setSpecieEnum(SpecieEnum specieEnum) {
        this.specieEnum = specieEnum;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Unity getUnity() {
        return unity;
    }

    public void setUnity(Unity unity) {
        this.unity = unity;
    }
}
