package br.com.tt.petshop.model.dto.animal;

import br.com.tt.petshop.enumeration.SpecieEnum;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class AnimalRequestDTO {

    private String name;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate birthDate;
    @Enumerated(EnumType.STRING)
    private SpecieEnum specieEnum;
    private Long idClient;

    public AnimalRequestDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public SpecieEnum getSpecieEnum() {
        return specieEnum;
    }

    public void setSpecieEnum(SpecieEnum specieEnum) {
        this.specieEnum = specieEnum;
    }

    public Long getIdClient() {
        return idClient;
    }

    public void setIdClient(Long idClient) {
        this.idClient = idClient;
    }
}
