package br.com.tt.petshop.controller;

import br.com.tt.petshop.exception.BusinessException;
import br.com.tt.petshop.model.dto.client.ClientRequestDTO;
import br.com.tt.petshop.service.ClientService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class ClientController {

    private final ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("sistema", "Client: ");
        model.addAttribute("clients", clientService.findAll());
        return "index";
    }

    @GetMapping("/add-client")
    public String addClient(Model model) {
        return "add-client";
    }

    @PostMapping("/client-form")
    public RedirectView clientForm(@ModelAttribute ClientRequestDTO client) throws BusinessException {
        clientService.save(client);
        return new RedirectView("/");
    }

    @GetMapping("/delete-client")
    public RedirectView deletetForm(@RequestParam Long clientId) {
        clientService.delete(clientId);
        return new RedirectView("/");
    }
}
