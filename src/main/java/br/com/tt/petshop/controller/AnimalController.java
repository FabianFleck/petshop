package br.com.tt.petshop.controller;

import br.com.tt.petshop.exception.BusinessException;
import br.com.tt.petshop.model.entity.Animal;
import br.com.tt.petshop.model.entity.Client;
import br.com.tt.petshop.service.AnimalService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
public class AnimalController {

    private final AnimalService animalService;

    public AnimalController(AnimalService animalService) {
        this.animalService = animalService;
    }

    @GetMapping("/list-animal")
    public String listAnimal(Model model, @RequestParam Long clientId) {
        model.addAttribute("animals", animalService.findByClientId(clientId));
        return ("/list-animal");
    }

    @GetMapping("/cliente/{clientId}/add-animal")
    public String animals(Model model, @PathVariable Long clientId) {
        model.addAttribute("specieEnumList", listSpecieEnum());
        model.addAttribute("clientId", clientId);
        return ("/add-animal");
    }

    @PostMapping("/cliente/{clientId}/animals")
    public RedirectView addAnimal(@ModelAttribute Animal animal, @PathVariable Long clientId, Model model) {
        try {
            animal.setClient(new Client(clientId));
            animalService.save(animal);
        } catch (BusinessException e) {
            e.printStackTrace();
            model.addAttribute("message", "Campos Inválidos");
        }
        return new RedirectView("/list-animal?clientId=" + clientId);
    }

    private List<String> listSpecieEnum() {
        return animalService.listSpecieEnum();
    }
}
