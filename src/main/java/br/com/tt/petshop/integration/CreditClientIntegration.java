package br.com.tt.petshop.integration;

import br.com.tt.petshop.integration.dto.CreditDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "Credit", url = "https://imersao-credito-api.herokuapp.com/credito")
public interface CreditClientIntegration {

    @GetMapping("/{cpf}")
    CreditDTO verifyCredit(@PathVariable("cpf") String cpf);
}
