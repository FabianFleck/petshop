package br.com.tt.petshop.integration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreditDTO {

    @JsonProperty("situacao")
    private String situation;

    @JsonProperty("pontos")
    private String points;

    public CreditDTO() {
    }

    public String getSituation() {
        return situation;
    }

    public void setSituation(String situation) {
        this.situation = situation;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}
