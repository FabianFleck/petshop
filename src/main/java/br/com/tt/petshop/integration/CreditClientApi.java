package br.com.tt.petshop.integration;

import br.com.tt.petshop.exception.BusinessException;
import br.com.tt.petshop.integration.dto.CreditDTO;
import org.springframework.stereotype.Component;

@Component
public interface CreditClientApi {
    CreditDTO verifyCredit(String cpf) throws BusinessException;
}
