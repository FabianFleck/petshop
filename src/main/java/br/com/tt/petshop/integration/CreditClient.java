package br.com.tt.petshop.integration;

import br.com.tt.petshop.exception.BusinessException;
import br.com.tt.petshop.integration.dto.CreditDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Component
public class CreditClient implements CreditClientApi {

    private final RestTemplate restTemplate;
    private static final Logger LOGGER = LoggerFactory.getLogger(CreditClient.class);

    public CreditClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public CreditDTO verifyCredit(String cpf) throws BusinessException {
        URI uri = URI.create("https://imersao-credito-api.herokuapp.com/credito/" + cpf);
        try {
            return restTemplate.getForObject(uri, CreditDTO.class);
        } catch (HttpClientErrorException e) {
            LOGGER.info("Erro " + e);
            throw new BusinessException("erro");
        }
    }
}
