package br.com.tt.petshop.exception;

public class BusinessException extends Exception {

    public BusinessException(String messageError) {
        super(messageError);
    }
}
