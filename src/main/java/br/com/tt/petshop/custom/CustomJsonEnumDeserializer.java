package br.com.tt.petshop.custom;

import br.com.tt.petshop.enumeration.SpecieEnum;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;

public class CustomJsonEnumDeserializer extends JsonDeserializer<SpecieEnum> {

    @Override
    public SpecieEnum deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        String value = p.getText();
        return SpecieEnum.valueOfLabel(value);
    }
}
