package br.com.tt.petshop.service;

import br.com.tt.petshop.converter.client.ClientToClientResponseDTOConverter;
import br.com.tt.petshop.integration.CreditClientApi;
import br.com.tt.petshop.model.dto.client.ClientResponseDTO;
import br.com.tt.petshop.model.entity.Client;
import br.com.tt.petshop.repository.ClientRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceTest {

    @Mock
    private ClientRepository clientRepository;

    @Mock
    private ClientToClientResponseDTOConverter clientToClientResponseDTOConverter;

    @Mock
    private CreditClientApi creditClient;


    private ClientService clientService;



    @Test
    public void shouldFindAllFail() {
        List<Client> clientList = new ArrayList<>();
        List<ClientResponseDTO> clients = clientService.findAll();
        assertEquals(clientList, clients);
    }
}
