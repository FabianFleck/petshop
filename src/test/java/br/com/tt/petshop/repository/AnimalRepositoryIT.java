package br.com.tt.petshop.repository;

import br.com.tt.petshop.enumeration.SpecieEnum;
import br.com.tt.petshop.model.entity.Animal;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AnimalRepositoryIT {

    @Autowired
    private AnimalRepository repository;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void shouldFailFindByClientId() {
        List<Animal> animalList = repository.findByClientId(1L);
        assertEquals(0, animalList.size());
    }

    @Test
    @Sql("classpath:insert_rex.sql")
    public void shouldSuccessFindByClientId() {
        List<Animal> animalList = repository.findByClientId(133L);
        assertEquals(1, animalList.size());
        Animal animal = animalList.get(0);
        assertEquals("Rex", animal.getName());
        assertEquals(Long.valueOf(133), animal.getClient().getId());
        assertEquals(SpecieEnum.MAMMAL, animal.getSpecieEnum());
    }

    @Test
    @Sql("classpath:delete_rex.sql")
    public void shouldFailFindByName() {
        List<Animal> animalList = repository.findByName("Rex");
        assertEquals(0, animalList.size());
    }

    @Test
    @Sql("classpath:insert_rex.sql")
    public void shouldSuccessFindByName() {
        List<Animal> animalList = repository.findByName("Rex");
        assertEquals(1, animalList.size());
        Animal animal = animalList.get(0);
        assertEquals("Rex", animal.getName());
    }
}
